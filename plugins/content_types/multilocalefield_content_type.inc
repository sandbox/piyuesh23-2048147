<?php

$plugin = array(
  'title' => t('Multi Locale'),
 	'content_types' => 'multilocalefield_content_type',
  'single' => TRUE,
  'category' => array('Multi Locale Category'),
  'edit form' => 'multilocale_form_edit',
  'render callback' => 'multilocale_form_render',
	'required context' => array(new ctools_context_required(t('Multilocalecontext'), 'multilocalecontext'),new ctools_context_required(t('Node'), 'node')),
);

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */

//function multilocalefield_multilocalefield_content_type_content_type_render($subtype, $conf, $args, &$context) {

function multilocale_form_render($subtype, $conf, $args, &$context) {
  $block = new stdClass();
	$country_id = $context[0]->data->country;
  $country_values_result = db_select('country_field_values', 'cv')
													 ->fields('cv')
													 ->condition('nid', 1,'=')
													 ->condition('country_id', $country_id,'=')
													 ->execute()
													 ->fetchAll();
	//TODO: This entire logic has to be changed when the multiple fields are to be pulled in 												 
  $field_value = unserialize($country_values_result[0]->field_name_and_value);
  
  $field_name_with_bundle = $conf['multilocale_field_name'];
  $filtered_field_name = explode('--',$field_name_with_bundle);
  $field_name_from_configuration = $filtered_field_name[1];
  
  // Logic to render the data
	$block->content = $field_value[$field_name_from_configuration]; //TODO: this hardcoded field name has to change
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 */
function multilocale_form_edit($form, &$form_state) {
  $conf = $form_state['conf'];
	
	/** Configuration options available for a ctools content type **/
  $form['multilocale_field_name'] = array(
    '#title' => t('Select Field'),
    '#type' => 'select',
    '#description' => t('Please select the field.'),
    '#default_value' => !empty($conf['multilocale_field_name']) ? $conf['multilocale_field_name'] : '',
    '#options' => _process_multilocale_variable(),
  );

  return $form;
}

/**
 * 'Edit form' callback for the content type.
 */
function multilocale_form_edit_submit(&$form, &$form_state) {
  // submitting form creates $form_state['conf'] which is available in render function as $conf
  foreach (element_children($form) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

function _process_multilocale_variable(){
	$multilocale_switchable = variable_get('multilocale_switchable',array());
	$switchable_fields = array();
	foreach($multilocale_switchable as $val){
		$switchable_fields[$val] = $val; 	
	}
	return $switchable_fields;
}