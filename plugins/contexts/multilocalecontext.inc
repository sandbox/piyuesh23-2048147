<?php

/**
 * @file
 * Sample ctools context type plugin that shows how to create a context from an arg.
 *
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("MultiLocalecontext"),
  'description' => t('Multi Locale Context will pull Geo Sensitive Data.'),
  'context' => 'multilocalefield_context_create_multilocalecontext',  // func to create context
  'context name' => 'multilocalecontext',
  'keyword' => 'multilocalecontext',

  // Provides a list of items which are exposed as keywords.
  'convert list' => 'multilocalecontext_convert_list',
  // Convert keywords into data.
  'convert' => 'multilocalecontext_convert',

  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter some data to represent this "multilocalecontext".'),
  ),
);

/**
 * Create a context, either from manual configuration or from an argument on the URL.
 *
 * @param $empty
 *   If true, just return an empty context.
 * @param $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param $conf
 *   TRUE if the $data is coming from admin configuration, FALSE if it's from a URL arg.
 *
 * @return
 *   a Context object/
 */
function multilocalefield_context_create_multilocalecontext($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('multilocalecontext');
  $context->plugin = 'multilocalecontext';

  if ($empty) {
    return $context;
  }
  			
  if ($conf) {
    if (!empty($data)) {
      $context->data = new stdClass();
      // For this simple item we'll just create our data by stripping non-alpha and
      // adding '_from_configuration_item_1' to it.
      $context->data->country = $_COOKIE['country_id']; //The cookie will always be set courtesy to ip_to_country module
	    $context->data->description = t("Country ID that will be determined by IP address");
      $context->title = t("Multilocalecontext context from config");
      return $context;
    }
  }
  else {
    // $data is coming from an arg - it's just a string.
    // This is used for keyword.
    $context->title = $data;
    $context->argument = $data;
    // Make up a bogus context
    $context->data = new stdClass();
    $context->data->country =  $_COOKIE['country_id']; //The cookie will always be set courtesy to ip_to_country module

    // For this simple item we'll just create our data by stripping non-alpha and
    // adding '_from_multilocalecontext_argument' to it.
    $context->data->description = t("Country ID that will be determined by IP address");
    $context->arg_length = strlen($context->argument);
    return $context;
  }
}

/**
 * Provide a list of sub-keywords.
 *
 * This is used to provide keywords from the context for use in a content type,
 * pane, etc.
 */
function multilocalecontext_convert_list() {
	return array(
    'country' =>  $_COOKIE['country_id'], //The cookie will always be set courtesy to ip_to_country module
    'description' => t('Description'),
  );
}

/**
 * Convert a context into a string to be used as a keyword by content types, etc.
 */
function multilocalecontext_convert($context, $type) {
  switch ($type) {
    case 'country':
      return $context->data->country;
    case 'description':
      return $context->data->description;
  }
}