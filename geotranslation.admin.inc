<?php

/**
 * @file
 * Admin page callbacks for the geotranslation_fields module.
 */

/**
 * Menu callback for admin/config/geotranslation/country.
 */
function geotranslation_fields_admin_list_countries($form, &$form_state) {
  global $base_url;
  $options = array();
  $data = "";
  $header = array(
    'counter' => array('data' => t('Select All'), 'class' => array('delete-all')),
    'country_id' => array('data' => t('Country ID'), 'class' => array('delete-all')),
    'country_name' => array('data' => t('Country Name'), 'class' => array('delete-all')),
    'ccode' => array('data' => t('Country Code'), 'class' => array('delete-all')),
    'timezone' => array('data' => t('timezone'), 'class' => array('delete-all')),
    'edit' => array('data' => t('Edit'), 'class' => array('delete-all')),
  );

  $query = db_select('countries', 'c')->extend('TableSort');
  $query->fields('c', array('country_id',
                            'country_name',
                            'ccode',
                            'timezone',
                            'default_country'));
  $results = $query->execute();
  $inc = 0;
  while ($data = $results->fetchObject()) {
    if ($data->default_country == '1') {
      $country_name = $data->country_name;
      drupal_set_message('The default country is ' . $country_name . '.');
      drupal_set_message('You can change the ' . $country_name . ' to other country if you wish.');
    }
    $inc++;
    $options[$data->country_id] = array(
      '#attributes' => array('class' => array('draggable')),
      'counter' => '<span class="number">' . $inc . '</span>',
      'country_id' => $data->country_id,
      'country_name' => $data->country_name,
      'ccode' => $data->ccode,
      'timezone' => $data->timezone,
      'edit' => l(t('edit'), 'admin/config/geotranslation/country/' . $data->country_id . '/edit', array('query' => drupal_get_destination(), 'attributes' => array('class' => array('admin-edit-button')))),
    );
  }
  $form['select_list'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No content available.'),
  );
  $link = l(t('+ Add Country'), 'admin/config/geotranslation/country/add', array(
    'query' => drupal_get_destination(),
    'attributes' => array(
      'class' => array('button'),
      'style' => 'float:right'),
    )
  );
  $form['pre_header2'] = array(
    '#type' => 'item',
    '#markup' => $link,
  );
  $form['action'] = array(
    '#prefix' => '<div class="admin-cancel-submit">',
    '#suffix' => '</div>',
  );
  $form['action']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#attributes' => array(
      'class' => array('admin-submit'),'id' => array('admin-validate-delete'),
    ),
  );
  $form['#redirect'] = $base_url . '/' . $_GET['q'];
  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Validate of Country deletion submissions.
 */
function geotranslation_fields_admin_list_countries_validate($form, &$form_state) {
  $action = $form_state['values']['op'];
  switch ($action) {
    case 'Delete':
      if (!is_array($form_state['values']['select_list']) || !count(array_filter($form_state['values']['select_list']))) {
        form_set_error('', t('No items selected.'));
      }
      break;
  }
}
/**
 * Submit callback of Country deletion submissions.
 */
function geotranslation_fields_admin_list_countries_submit($form, &$form_state) {
  $action = $form_state['values']['op'];
  switch ($action) {
    case 'Delete':
      if (count(array_filter($form_state['values']['select_list'])) > 0) {
        $country_id = array_filter($form_state['values']['select_list']);
        foreach ($country_id as $value) {
          $num_deleted = db_delete('countries')
                ->condition('country_id', $value)
                ->execute();
        }
        drupal_set_message(t('country have been successfully deleted.'), 'status');
      }
      break;
  }
  drupal_goto($form['#redirect']);
}
/**
 * Function for adding the country.
 */
function geotranslation_fields_add_country($form, &$form_state) {
  global $account;
  global $user;
  $form = array();
  $form['country_name'] = array(
    '#type'             => 'textfield',
    '#description'      => t('Please make sure country name should be unique'),
    '#title'            => t('Country Name'),
    '#required' => TRUE,
  );
  $form['country_code'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Country Code'),
    '#description'      => t('Please make sure country code should be unique'),
    '#required' => TRUE,
  );
  $form['settings']['default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make this default Country'),
    '#default_value' => '0',
    '#description' => t('Please select this radio box for making this country default throughtout the site'),
  );
  $form['settings']['publish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Publish this Country'),
    '#default_value' => '0',
    '#description' => t('Please select this radio box for publishing this country'),
  );
  $form['settings']['timezone'] = array(
    '#type' => 'select',
    '#title' => t('Time zone'),
    '#default_value' => isset($account->timezone) ? $account->timezone : ($account->uid == $user->uid ? variable_get('date_default_timezone', '') : ''),
    '#options' => system_time_zones($account->uid != $user->uid),
    '#description' => t('Select the desired time and time zone.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Country'),
  );
  $form['#validate'][] = 'geotranslation_fields_add_country_validate';
  $form['#submit'][] = 'geotranslation_fields_add_country_submit';
  return $form;
}
/**
 * Validate add_country form submissions.
 */
function geotranslation_fields_add_country_validate($form, &$form_state) {
  $country_name = $form_state['values']['country_name'];
  $ccode = $form_state['values']['country_code'];
  $data = "";
  // This is for checking country name!
  $query = db_select('countries', 'c');
  $query->fields('c', array('country_name'))
    ->condition('c.country_name', $country_name)
    ->range(0, 1);
  $data = $query->execute()->fetchObject();

  // This is for checking country Code!
  $query1 = db_select('countries', 'c');
  $query1->fields('c', array('ccode'))
    ->condition('c.ccode', $ccode)
    ->range(0, 1);
  $data1 = $query1->execute()->fetchObject();

  if ($data->country_name) {
    form_set_error('', t('Country Name is already present, Plese put some another country.'));
  }
  elseif ($data1->ccode) {
    form_set_error('', t('Country Code is already present, Plese put some another Code.'));
  }
}

/**
 * Process add_country form submissions.
 */
function geotranslation_fields_add_country_submit($form, &$form_state) {
  if ($form_state['values']['default'] == '1') {
    $num_updated = db_update('countries')
        ->fields(array('default_country' => '0'))
        ->condition('default_country', '1')
        ->execute();
  }
  $nid = db_insert('countries')
    ->fields(array(
        'country_name' => $form_state['values']['country_name'],
        'ccode' => $form_state['values']['country_code'],
        'default_country' => $form_state['values']['default'],
        'publish' => $form_state['values']['publish'],
        'timezone' => $form_state['values']['timezone'],
      ))->execute();
}

/**
 * Process editing the country form.
 */
function geotranslation_fields_admin_country_edit($form, &$form_state) {
  $arg = arg();
  global $user;
  $account = $user;
  $country_id = $arg[4];
  $form = drupal_get_form('add_country');
  $query = db_select('countries', 'c');
  $query->fields('c', array(
                    'country_name',
                    'ccode',
                    'default_country',
                    'publish',
                    'timezone'))
    ->condition('c.country_id', $country_id)
    ->range(0, 1);
  $data = $query->execute()->fetchObject();
  $form = array();
  $form['country_name'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Country Name'),
    '#description'      => t('Please make sure country name should be unique'),
    '#default_value'    => $data->country_name,
    '#required' => TRUE,
  );
  $form['country_code'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Country Code'),
    '#description'      => t('Please make sure country code should be unique'),
    '#default_value'    => $data->ccode,
    '#required' => TRUE,
  );
  $form['settings']['default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make this default Country'),
    '#default_value' => $data->default_country,
    '#description' => t('Please select this radio box for making this country default throughtout the site'),
  );
  $form['settings']['publish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Publish this Country'),
    '#default_value' => $data->publish,
    '#description' => t('Please select this radio box for publishing this country'),
  );
  $form['settings']['timezone'] = array(
    '#type' => 'select',
    '#title' => t('Time zone'),
    '#default_value' => $data->timezone,
    '#options' => system_time_zones($account->uid != $user->uid),
    '#description' => t('Select the desired time and time zone.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Edit Country'),
  );
  $form['#validate'][] = 'geotranslation_fields_admin_country_edit_validate';
  $form['#submit'][] = 'geotranslation_fields_admin_country_edit_submit';
  return $form;
}

/**
 * Process edit country form submissions .
 */
function geotranslation_fields_admin_country_edit_submit($form, &$form_state) {
  $arg = arg();
  if ($form_state['values']['default'] == '1') {
    $num_updated = db_update('countries')
        ->fields(array('default_country' => '0'))
        ->condition('default_country', '1')
        ->execute();
  }
  $num_updated = db_update('countries')
          ->fields(array(
            'country_name' => $form_state['values']['country_name'],
            'ccode' => $form_state['values']['country_code'],
            'default_country' => $form_state['values']['default'],
            'publish' => $form_state['values']['publish'],
            'timezone' => $form_state['values']['timezone'],
          ))
          ->condition('country_id', $arg[4])
          ->execute();
}

/**
 * Process edit country form validation.
 */
function geotranslation_fields_admin_country_edit_validate($form, &$form_state) {

}
